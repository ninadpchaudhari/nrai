(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/**
 * Created by ninadpchaudhari on 06/10/15.
 */
"use strict";

function validateNraiID(id) {
    if (id.match(/(SHM|SHF)([123]0|[012][1-9]|31)(0[1-9]|1[012])(19[0-9]{2}|2[0-9]{3})\d\d/g)) {
        return true;
    }
    return false;
};

function fetchNraiID(id) {
    var url = "/api/athleteInfo/" + id;
    var d = $.Deferred();
    var request = $.ajax({
        url: url,
        method: "GET",
        dataType: "json"
    });
    request.done(function (response) {
        //console.log("fetching NraiID Done response: ");
        //console.log(response);
        if (response == "[ ]") return d.resolve("");
        d.resolve(response);
    });
    request.fail(function (jqXHR) {
        console.log(jqXHR);
        d.reject();
    });
    return d;
};
$(document).ready(function () {
    $("#shooterID").show(); // Will be hide for production
    //$('.modal-trigger').leanModal();
    $("#displayEvents").hide(); // Will be hide for production

    $("#contactInfo").hide();
    $("#welcomeText").hide();
    if ($("select#match_id option").length == 2) {
        $("select#match_id option:eq(1)").attr("selected", "selected");
        $("#shooterID").show();
    }
});
$("#welcomeText").on("display", function (event, athleteInfo) {
    //console.log("triggred");
    $(this).show();
    $(this).html("Welcome " + athleteInfo.shooterName + " !");
});
$("#displayEvents").on("loadMe", function () {
    $(this).show();
    $(this).html(require("./partials/_loadingCircle.html"));
    //console.log($("#shooterID").val().toUpperCase());
    $.when(fetchNraiID($("#shooterID").val().toUpperCase())).done(function (athleteInfo) {
        if (athleteInfo != "") {

            var match_id = $("select#match_id :selected").val();
            var url = "/partials/athleteCompatibleEvents/" + match_id + "/" + athleteInfo.shooterID;
            var request = $.ajax({
                url: url,
                method: "GET",
                dataType: "html"
            });
            request.done(function (msg) {
                $("#welcomeText").trigger("display", [athleteInfo]);
                $("#displayEvents").html(msg);
                $('.collapsible').collapsible({
                    accordion: false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
                });
            });
            request.fail(function (jqXHR, textStatus) {
                console.log(jqXHR);
                $("#displayEvents").html("Request Failed " + jqXHR.status + " " + jqXHR.statusText);
            });
        } else {
            console.log("loadMe Trigger Called but Nrai ID is not valid if statement :");
            $("#displayEvents").html("Error : Nrai ID Not Found in Database");
            return 1;
        }
    });
}).on("change", "input[type=checkbox]", function () {
    $("#contactInfo").trigger("eventsSelected");
});

$("select#match_id").change(function () {
    $("#shooterID").show();
});
$('.collapsible').collapsible({
    accordion: false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
});
$("#shooterID").keyup(function () {
    var value = $(this).val().toUpperCase();
    $(this).val(value);
    if (value.length < 3 || value.length >= 13) $(this).val(value.substring(0, 13));
    if (validateNraiID(value) == true) {
        $(this).removeClass("invalid");
        $("#displayEvents").trigger("loadMe");
    } else {
        $(this).addClass("invalid");
        $("#displayEvents").hide();
        //console.log("Not Valid");
        //console.log(value);
    }
}).on("focusout", function () {
    var value = $(this).val().toUpperCase();
    if (validateNraiID(value) == true) {
        $("#shooterID").removeClass("invalid");
    } else {
        $("#shooterID").addClass("invalid");
    }
});

$("#contactInfo").on("eventsSelected", function () {
    $(this).show();
});
function validateMobileNumber(mobileNo) {
    if (/^\d{10}$/.test(mobileNo)) return true;else return false;
};
$("#formSubmit").submit(function (e) {
    if (validateMobileNumber($(this).find('input[name="mobileNo"]').val()) == false) {
        console.log($(this).find('input[name="mobileNo"]').val());
        e.stopImmediatePropagation();
        e.preventDefault();
    } else {
        var events = $("#displayEvents input:checkbox:checked").map(function () {
            return $(this).prop("name");
        }).toArray();
        var eventNames = $("#displayEvents input:checkbox:checked").map(function () {
            return $(this).attr("data-eventname");
        }).toArray();

        $(this).find('input[name="events"]').val(events);
        $(this).find('input[name="eventNames"]').val(eventNames);
        $(this).find('input[name="match_id"]').val($("select#match_id option:selected").val());
        $(this).find('input[name="shooterID"]').val($("#shooterID").val());
    }

    //console.log($(this).find('input[name="shooterID"]').val());
    //console.log($(this).find('input[name="match_id"]').val());
});
$("#findShooterIDLink").on('click', function () {

    var url = "/partials/findShooters";
    var request = $.ajax({
        url: url,
        method: "GET",
        dataType: "html"
    });
    request.done(function (msg) {
        $("#findShooterID").html(msg).openModal();
    });

    request.fail(function (jqXHR, textStatus) {
        console.log(jqXHR);
        $("#findShooterID").html("Request Failed " + jqXHR.status + " " + jqXHR.statusText).openModal();
    });
});

},{"./partials/_loadingCircle.html":2}],2:[function(require,module,exports){
module.exports = '<div class="center">\n\n    <div class="preloader-wrapper big active">\n        <div class="spinner-layer spinner-blue">\n            <div class="circle-clipper left">\n                <div class="circle"></div>\n            </div><div class="gap-patch">\n            <div class="circle"></div>\n        </div><div class="circle-clipper right">\n            <div class="circle"></div>\n        </div>\n        </div>\n\n        <div class="spinner-layer spinner-red">\n            <div class="circle-clipper left">\n                <div class="circle"></div>\n            </div><div class="gap-patch">\n            <div class="circle"></div>\n        </div><div class="circle-clipper right">\n            <div class="circle"></div>\n        </div>\n        </div>\n\n        <div class="spinner-layer spinner-yellow">\n            <div class="circle-clipper left">\n                <div class="circle"></div>\n            </div><div class="gap-patch">\n            <div class="circle"></div>\n        </div><div class="circle-clipper right">\n            <div class="circle"></div>\n        </div>\n        </div>\n\n        <div class="spinner-layer spinner-green">\n            <div class="circle-clipper left">\n                <div class="circle"></div>\n            </div><div class="gap-patch">\n            <div class="circle"></div>\n        </div><div class="circle-clipper right">\n            <div class="circle"></div>\n        </div>\n        </div>\n    </div>\n\n</div>';
},{}]},{},[1]);

//# sourceMappingURL=form.js.map
