<?php

use Illuminate\Database\Seeder;

class linkUnitsAndAthletes extends Seeder
{
    /**
     * Should be run AFTER ALL THE IMPORTS
     * Populated representing_unit column of Athletes and normalizes the stateOfRep in
     * same table ,corrects some mistakes
     *
     * @return void
     */
    public function run()
    {
        //
        DB::transaction(function(){
            $this->command->info("Linking Athletes and Units tables Starting");
            $athletes = \App\Athlete::all();
            foreach($athletes as $athlete){
                $this->command->info($athlete->shooterID);
                if($athlete->stateOfRep == "ASSM") {
                    $athlete->stateOfRep = "ASSAM";
                }
                if($athlete->stateOfRep == "TELANGANA STATE RIFLE ASSOCIATION (HYD)"){
                    $athlete->stateOfRep = "TELANGANA STATE RIFLE ASSOCIATION";
                }
                if($athlete->stateOfRep == "CH"){
                    $athlete->stateOfRep = "chandigarh";
                }
                $unit = \App\Unit::where('name','like',strtolower($athlete->stateOfRep))
                    ->orWhere('association_name','like',strtolower($athlete->stateOfRep))
                    ->first();
                $athlete->representing_unit =$unit->id;
                $athlete->save();
            }
            $this->command->info("Linking Athletes and Units tables Completed ");
        });

    }
}
