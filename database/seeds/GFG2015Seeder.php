<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class GFG2015Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */



    public function run()
    {
        //
        Model::unguard();
        ini_set('memory_limit', '-1');
        DB::transaction(function(){

            $DBNAME = 'gfg2015';
            //Deleting any null values
            DB::delete("delete from `" . $DBNAME . "`.participation where shooterID IS NULL or shooterID = ''");
            //Importing comMaster => Competition Data

            $compInfo = DB::table($DBNAME.'.compMaster')->first();
            $newInfo['name'] = $compInfo->compName;
            $newInfo['short_name'] = $compInfo->compCode;
            $newInfo['place'] = $compInfo->place;
            $newInfo['start_date'] = $compInfo->dtFr;
            $newInfo['end_date'] = $compInfo->dtTo;
            $newInfo['year'] = 2015;
            $newInfo['stateVerificationRequired'] = true;
            // this match_id will be used for adding participations
            $this->command->info(implode('-',$newInfo));
            $match = \App\Match::create($newInfo);
            $this->command->info("Match {$match->name} added");
///*

            //Importing Athletes if any new ;Only importing fields that are used by the Atlete model +photo for saving
                        $thisCompAthletes = DB::table($DBNAME.'.shooterm')->select(
                            'cardNo', 'shooterID', 'idCount', 'shooterName', 'motherName', 'fatherName', 'address', 'city', 'pin', 'state', 'education', 'stateOfRep', 'eventRifle', 'eventPistol', 'eventShotgun', 'sex', 'POB', 'DOB', 'photoAvail', 'signAvail', 'contact', 'email', 'provisionalShooter', 'photo'
                        )->get();

                        foreach($thisCompAthletes as $thisCompAthlete){
                            $thisCompAthlete = (array) $thisCompAthlete;

                            if($thisCompAthlete['provisionalShooter'] == '1'){


                                //
                                //DB::table($DBNAME.'.shooterm')
                                //    ->where('shooterID',$old_shooterID)
                                //    ->update(['shooterID' => $thisCompAthlete['shooterID']]);
                                //DB::table($DBNAME.'.Participation')
                                //    ->where('shooterID',$old_shooterID)
                                //    ->update(['shooterID' => $thisCompAthlete['shooterID']]);
                            }


                            /**
                             * Searching for Athlete with same ID ,
                             * If Found=> Update
                             * Else Create
                             */
///*
                            else{
                                $this->command->info("Entering Normal Shooter clause for {$thisCompAthlete['shooterID']} ");

                                $this->saveImage($thisCompAthlete['photo'], $thisCompAthlete['shooterID']);


                                unset($thisCompAthlete['photo']);
                                $athlete = \App\Athlete::where('shooterID',$thisCompAthlete['shooterID'])->first();
                                unset($thisCompAthlete['provisionalShooter']);
                                if($athlete == null){

                                    $athlete = \App\Athlete::create($thisCompAthlete);
                                }
                                else{
                                    $this->command->info("Updating{$thisCompAthlete['shooterID']}");
                                    $athlete->update($thisCompAthlete);

                                }
                                //Saving Photo.Since only non-provisional shooters have it,Its in else clause
                                //file_put_contents(storage_path("shooter_i_d_photos".$athlete->shooterID.".jpg"),$thisCompAthlete['photo'] );
                                //$athlete->photoAvail = true;
                                $athlete->save();

                            }

                        }

            /**
             * Importing all Participations for the match
             *
             */
            $participations = collect(DB::table($DBNAME . '.Participation')->select("*")->get());

            $participationsCount = DB::table($DBNAME.'.Participation')->select(
                'matchNo','matchName','qlyScore','matchType','shooterID','cptrCode','team','wildcard','participatingState','total','shotgunTotal','FsubTotal','shotgunFinalTotal','Rank'
            )->count();
            foreach($participations as $participation) {
                $this->command->info("Remaning Participations : {$participationsCount}");
                $event = \App\Event::where(['match_id' => $match->id, 'name' => $participation->matchNo])->first();
                if ($event == null) {

                    $event['match_id'] = $match->id; // FROM the match added before
                    $event['name'] = $participation->matchNo;
                    $this->command->info("On participation loop , eventname : {$event['name']}");
                    $event['class'] = (string)\App\Event::decodeEvent($participation->matchName, 'classes');
                    $event['type'] = \App\Event::decodeEvent($participation->matchName, 'types');
                    $event['gender'] = \App\Event::decodeEvent($participation->matchName, 'genders');
                    $event['nat_civil'] = \App\Event::decodeEvent($participation->matchName, 'nat_civil');
                    $event['category'] = \App\Event::decodeEvent($participation->matchName, 'categories');
                    $event['consider_for_qualification'] = true;
                    echo "\n";
                    print_r($event);
                    $event = \App\Event::create($event);
                    $this->command->info("Event added id: {$event->id}");
                }
                $shooterInfo = null;
                $shooterInfo = DB::table($DBNAME . '.shooterm')->select()->where('shooterID', "=", $participation->shooterID)->get();
                $shooterInfo = $shooterInfo["0"];

                $isProvisional = (bool) $shooterInfo->provisionalShooter;
                if($isProvisional){
                    $ps= new \App\ProvisionalScore();
                    $this->command->info("Adding provisionalScore shooterID " . $shooterInfo->shooterID);
                    $ps->shooterID = $shooterInfo->shooterID;
                    $ps->name = $shooterInfo->shooterName;

                    $ps->cptrCode = $participation->cptrCode;
                    $ps->DOB = $shooterInfo->DOB;

                    $ps->event_id = $event->id;
                    $ps->match_id = $match->id;

                    //Since the original DB does not have shot wise scoring

                    $shots = array();

                    $shots[0] = $participation->ATotal;
                    $shots[1] = $participation->BTotal;
                    $shots[2] = $participation->CTotal;
                    $shots[3] = $participation->DTotal;
                    if ($participation->ETotal > 0) $shots[4] = $participation->ETotal;
                    if ($participation->FTotal > 0) $shots[5] = $participation->FTotal;

                    $ps->shots = $shots;
                    $unit = \App\Unit::where('name',strtolower($participation->participatingState))->first();
                    $ps->representing_unit = $unit->id;
                    $ps->score = $participation->total;
                    $ps->tenx = $participation->inner10;
                    $ps->save();
                }
                else{
                    $this->command->info("Adding score shooterID " . $shooterInfo->shooterID);
                    $score = new \App\Score();

                    $score->shooterID = $shooterInfo->shooterID;

                    $score->cptrCode = $participation->cptrCode;
                    $shots = array();//VERY IMP ! since the array should be zeroed at every round !

                    $shots[0] = $participation->ATotal;
                    $shots[1] = $participation->BTotal;
                    $shots[2] = $participation->CTotal;
                    $shots[3] = $participation->DTotal;
                    if ($participation->ETotal > 0) $shots[4] = $participation->ETotal;
                    if ($participation->FTotal > 0) $shots[5] = $participation->FTotal;

                    $score->shots = $shots;


                    $score->event_id = $event['id'];
                    $score->match_id = $match->id;
                    $score->score = $participation->total;
                    //$score->photoAvail = $participation->photoAvail = true;
                    $unit = \App\Unit::where('name',strtolower($participation->participatingState))->first();

                    $score->representing_unit = $unit->id;

                    $score->tenx = $participation->inner10;


                    $score->save();
                    $this->addFinalScores($participation, $score->id);
                }
                $participationsCount--;

                $event = null;
                $score= null;
            }
            //DB::update('update '.$DBNAME.'.Participation set shooterID = right(shooterID,13)');
            //DB::update('update '.$DBNAME.'.shooterm set shooterID = right(shooterID,13)');
        });
        Model::reguard();
    }

    public function saveImage($imageString, $shooterID)
    {
        if (file_exists(storage_path("shooter_i_d_photos/" . $shooterID . ".jpg"))) return;
        try {
            $image = imagecreatefromstring($imageString);
            if ($image != FALSE) {
                //VALID IMAGE
                imagejpeg($image, storage_path("shooter_i_d_photos/" . $shooterID . ".jpg"));
            } else {
                //noting down the corrupt image shooterIDs
                throw New Exception();
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            file_put_contents(storage_path("shooter_i_d_photos/" . "invaidImages.txt"), $shooterID, FILE_APPEND | LOCK_EX);
        }
    }

    public function addFinalScores($participation, $scoreID)
    {
        //$participation->toArray();
        $participation = collect($participation)->toArray();

        $FA = 'FA';
        $finalShots = [];
        for ($i = 1; $i <= 20; $i++) {
            array_push($finalShots, $participation[$FA . $i]);
        }

        $finalScore = New \App\FinalScore();
        $finalScore->score_id = $scoreID;
        $finalScore->shots = $finalShots;
        $finalScore->finalScore = $participation['FsubTotal'];
        $finalScore->tie = $participation['Ftie'];
        $finalScore->rank = $participation['Rank'];
        $finalScore->save();

    }
}
