<?php

use Illuminate\Database\Seeder;

class Create59NSCC extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return int
     */
    public function addMatch()
    {
        $match = \App\Match::firstOrNew(['short_name'=>'59NSCC']);
        $match->name = "59th National Shooting Championship";
        $match->year = 2015;
        $match->place = "Delhi";
        $match->start_date = \Carbon\Carbon::create(2015,12,1);
        $match->end_date = \Carbon\Carbon::create(2015,12,15);
        $match->last_date = \Carbon\Carbon::create(2015,12,1);
        $match->stateVerificationRequired = true;
        $match->save();
        return $match->id;
    }
    public function run()
    {
        //

        $this->command->info("Starting to create 59NSCC ");

        DB::transaction(function(){

            $this->command->info("Locating if it exists and adding if not");
            $match_id=$this->addMatch();
            $this->command->info("Removing all events");
            $deletedRows = \App\Event::where('match_id',$match_id)->delete();
            $this->command->info($deletedRows." Deleted");

            $this->command->info("Seeding Events from 59nscc_events table in SeederDB");

            $events = DB::table(env('MYSQL_SEEDER_DB', 'seedDB').'.59nscc_events')->get();
            foreach($events as $event)
            {
                $event->match_id = $match_id; //For Setting the match_id as it is in system
                $event->issf_qualification_score = 0;//For making the software lookup for default vaules from qual_scores table
                $event->nr_qualification_score = 0;//Same as Above
                $event->max_score =0;//Same as above
                $event = (array) $event;
                \App\Event::create($event);
            }
        });$this->command->info("Created 59NSCC ");
    }
}
