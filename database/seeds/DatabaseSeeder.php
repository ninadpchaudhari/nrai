<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call(UserTableSeeder::class);
        $this->call(AddSuperAdmin::class);
        $this->call(AthleteSeeder::class);
        $this->call(UnitsTableSeeder::class);
        $this->call(GFG2015Seeder::class);
        $this->call(NSCC2014ShotgunSeeder::class);
        $this->call(linkUnitsAndAthletes::class);
        $this->call(loginForUnits::class);
        //$this->call(ImportQualScores::class); // Adding QualScores
        Model::reguard();
    }
}
