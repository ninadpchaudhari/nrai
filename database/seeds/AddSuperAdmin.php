<?php

use Illuminate\Database\Seeder;

class AddSuperAdmin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $this->command->info("Adding the superAdmin user");
        $user = New \App\User();
        $user->name = "NRAI president";
        $user->email = env("SUPERADMIN");
        $user->password = bcrypt("NINAD111");
        //$user->rights = array('register-users');
        //$user->mobileNo = "9405441318";
        $user->remember_token = str_random(10);
        $user->unit_id = 0;
        $user->save();
        $this->command->info("Super Admin added");


    }
}
