<?php

use Illuminate\Database\Seeder;

class loginForUnits extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::transaction(function (){
            $this->command->info("Adding Logins for All Units");
            $units = \App\Unit::all();
            foreach($units as $unit){

                $user = new App\User([
                    'name'=> $unit->name,
                    'email' => strtr($unit->abbreviation,array('.'=>'')).'@thenrai.in',
                    'password' => bcrypt('NINAD111'),
                    'unit_id' => $unit->id
                ]);
                $user->save();
            }
        });
    }
}
