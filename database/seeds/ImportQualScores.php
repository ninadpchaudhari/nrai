<?php

use Illuminate\Database\Seeder;

class ImportQualScores extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \App\QualScore::unguard();
        DB::transaction(function(){
            $this->command->info("QualScores Seeder Starting ");
            $results = DB::table(env('MYSQL_SEEDER_DB', 'seedDB').'.qual_scores')->get();
            foreach($results as $result){
                $result = array_except((array)$result,['id']);

                \App\QualScore::create($result);
            }
            $this->command->info("QualScores Seeder Done ");

        });
        \App\QualScore::reguard();
    }
}
