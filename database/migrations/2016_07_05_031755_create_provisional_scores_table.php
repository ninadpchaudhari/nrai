<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvisionalScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('provisional_scores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('shooterID'); //Absoute useless
            $table->string('name');
            $table->integer('cptrCode')->unsigned();
            $table->dateTime('DOB');
            $table->string('sex');
            //Moved to Forms Table
            //$table->boolean('inTeam');
            //Moved to Forms
            //$table->boolean('isWildCard');
            $table->integer('event_id')->unsigned();
            $table->integer('match_id')->unsigned();
            $table->decimal('score', 6, 2)->nullable();
            $table->decimal('penaltyOrTie', 2, 1)->default(0); //+ve=>Tie addition ive=>penalty
            $table->integer('representing_unit')->unsigned();
            $table->integer('rank')->unsigned(); //rank , calculated later
            //this needs to be calculateed in new table every competition
            $table->string('recordRemark')->nullable();
            $table->string('shots')->nullable(); // Seriaized class of the score
            $table->string('series')->nullable();
            $table->string('remark', 5)->nulable();
            $table->integer('tenx');
            //$table->integer('relay_no')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('representing_unit')->references('id')->on('units');
            $table->foreign('event_id')->references('id')->on('events');
            $table->foreign('match_id')->references('id')->on('matches');
            //$table->foreign('relay_id')->references('id')->on('relays');
            $table->index('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop("scores");
    }
}
