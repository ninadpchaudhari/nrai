<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinalScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create("final_scores", function (Blueprint $table) {
            $table->increments('id');
            $table->integer('score_id')->unsigned();
            $table->string('shots'); //all shots
            $table->decimal('finalScore', 6, 2)->nullable(); // Finals score (not total)
            $table->decimal('tie');
            $table->string('rank')->nullable();
            $table->foreign('score_id')->references('id')->on('scores');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop("final_scores");
    }
}
