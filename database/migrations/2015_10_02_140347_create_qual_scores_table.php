<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQualScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qual_scores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('class');
            $table->string('type'); // ISSF/NR/FOREIGN_NATIONALS
            $table->string('gender'); //Men/Women/Common
            $table->string('category'); // senior/junior/youth/handicapped/vertrian/services/mqs
            $table->decimal('qual_score',6,2);
            $table->decimal('max_score',6,2);
            $table->boolean('isDecimal');  // true for decimal counting
            $table->string('remarks')->nullable();//Extra field
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('qual_scores');
    }
}
