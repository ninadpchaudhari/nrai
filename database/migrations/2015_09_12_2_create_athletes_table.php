<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
//use Illuminate\Support\Facades\Schema;

class CreateAthletesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if( ! Schema::hasTable('athletes')) {
            Schema::create('athletes', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('cardNo')->unsigned();
                $table->string('shooterID')->unique();
                $table->integer('idCount')->unsigned();
                $table->string('shooterName');
                $table->string('motherName');
                $table->string('fatherName');
                $table->string('address');
                $table->string('city');
                $table->integer('pin')->unsigned();
                $table->string('state');
                $table->string('education');
                $table->string('stateOfRep');
                $table->boolean('eventRifle');
                $table->boolean('eventPistol');
                $table->boolean('eventShotgun');
                $table->string('sex');
                $table->string('POB');
                $table->dateTime('DOB');
                $table->boolean('photoAvail'); // photos are stored with shooterID.jpg
                $table->boolean('signAvail');
                $table->string('contact');
                $table->string('email');
                $table->timestamps();
                //Adding Representing Unit to athletes
                $table->unsignedInteger('representing_unit')->nullable();
                $table->foreign('representing_unit')->references('id')->on('units');
                $table->index('shooterID');
                $table->index('shooterName');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('athletes');
    }
}
