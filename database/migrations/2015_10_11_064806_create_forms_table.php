<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('amount')->unsigned();
            $table->string('shooterID');
            $table->integer('representing_unit')->unsigned();
            $table->integer('verified_by')->unsigned()->default(0);// 2=>By Nrai 1=>By state 0=>No
            $table->integer('match_id')->unsigned();
            $table->string('events');
            $table->string('mobileNo', 10);
            $table->integer('mobileNo_verified')->default(0);
            $table->string('email')->nullable();
            $table->integer('email_verified')->default(0);
            $table->integer('payment_option')->unsigned()->default(0); //1 ->cash 2->online
            $table->string('payment_evidence')->default("");
            $table->string('clientIP', 45);
            $table->timestamps();

            $table->foreign('shooterID')->references('shooterID')->on('athletes');
            $table->foreign('representing_unit')->references('id')->on('units');
            $table->foreign('match_id')->references('id')->on('matches')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('forms');
    }
}
