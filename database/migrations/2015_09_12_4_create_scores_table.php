<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('shooterID');
            $table->integer('cptrCode')->unsigned();
            $table->string('shots')->nullable();
            $table->string('series')->nullable();
            
            //Moved to Forms Table
            //$table->boolean('inTeam');
            //Moved to Forms
            //$table->boolean('isWildCard');
            $table->integer('event_id')->unsigned();
            $table->integer('match_id')->unsigned();
            $table->decimal('score',6,2)->nullable();
            $table->decimal('penaltyOrTie', 2, 1)->default(0); //+ve=>Tie addition ive=>penalty
            //Moved to FinalScores table
            //$table->decimal('finalScore', 6, 2)->nullable(); // Finals score (not total)
            $table->integer('representing_unit')->unsigned();
            $table->string('rank')->nullable(); //rank , calculated later
            //this needs to be calculateed in new table every competition read notes
            $table->string('recordRemark')->nullable();
            $table->string('remark', 5)->nullable();
            $table->integer('tenx');
            //$table->integer('relay_no')->unsigned()->nullable();
            $table->timestamps();
            $table->unique(['shooterID', 'match_id', 'event_id']);
            
            $table->foreign('representing_unit')->references('id')->on('units');
            $table->foreign('shooterID')->references('shooterID')->on('athletes');
            $table->foreign('event_id')->references('id')->on('events');
            $table->foreign('match_id')->references('id')->on('matches');
            //$table->foreign('relay_id')->references('id')->on('relays');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("scores");
    }
}

