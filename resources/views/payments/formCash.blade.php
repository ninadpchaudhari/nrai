@extends('app')

@section('content')
    <h3 class="center">Payment for Form ID : {{ $form->id }} ; shooter ID : {{$form->shooterID}}</h3>
    <hr>
    <h3>Please Visit your State Association : {{ $unit->name }}</h3>
    <h4>{{$unit->association_name}}</h4>
    <h3>Payable Amount : {{ $form->amount }}</h3>
    <h3>Last Date for Payment : {{ $endDate }} ,  </h3>
    <p>Thank you for your entry form.</p>
    <p> Note , Your entry is subject to approval of your association.</p>
    <p> For any queries please contact the unit/state you are representing.</p>
@endsection
@section('footer')
@endsection