@extends('app')

@section('content')
    <h3 class="center">Payment for Form ID : {{ $form->id }}</h3>
    <h4>Payable Amount : {{ $form->amount }}</h4>
    <p>Please select a payment method :</p>
    <a href="{{route('formPaymentCash')}}">1) Cash at the State Association</a> </br>
    
    <form action={{ route("catchOnlinePayment")}} method="POST">
    {{ csrf_field() }}
<!-- Note that the amount is in paise = 50 INR -->
<script
    src="https://checkout.razorpay.com/v1/checkout.js"
    data-key="rzp_test_RsHCGrCwUWltOi"
    data-amount="{{ $form->amount*100 }}"
    data-buttontext="Pay Online"
    data-name="National Rifle Association of India"
    data-description="Form fees"
    data-image="http://nrai.app/images/logo-small.png"
    data-prefill.name="Harshil Mathur"
    data-prefill.email="support@razorpay.com"
    data-theme.color="#F37254"
></script>
<input type="hidden" value="Hidden Element" name="hidden">
</form>
    @endsection
@section('footer')
    @endsection