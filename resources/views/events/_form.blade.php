{{ csrf_field() }}
<input type="hidden" name="match_id" id="match_id" value="{{isset($match)? $match->id : $event->match_id}}">
<br>
<div class="input-field">
    <input
            type="text"
            id="name" name="name"
            placeholder="Eg : N-01,N-02"
            value="{{ $event->name or "" }}"
            >
    <label for="name">Name</label>
</div>


<select name="class" id="class" class="browser-default">
    <option value="" disabled selected>Select the Class</option>
    @foreach($decodeArray['classes'] as $class)
        <option value="{{$class}}"
                @if(isset($event) ? $class == $event->class : false)
                selected
                @endif
                >{{ $class }}</option>
    @endforeach
</select>
<label for="class">Select Class</label>

<label for="type">Type of Match</label>
<select name="type" id="type" class="browser-default">
    <option value="" disabled selected>Select Type</option>
    @foreach($decodeArray['types'] as $type)
        <option value="{{$type}}"
                @if(isset($event) ? $type == $event->type :  false)
                    selected
                @endif
                >{{ $type }}</option>
    @endforeach
</select>


<label for="gender">Gender</label>
<select name="gender" id="gender" class="browser-default">
    <option value="" disabled selected>Select Gender</option>
    @foreach($decodeArray['genders'] as $gender)
        <option value="{{$gender}}"
                @if(isset($event) ? $gender == $event->gender : false)
                    selected
                @endif
                >{{ $gender }}</option>
    @endforeach
</select>


<label for="nat_civil">National/Civilian</label>
<select name="nat_civil" id="nat_civil" class="browser-default">
    <option value="" selected disabled>Select Nat/Civil</option>
    @foreach($decodeArray['nat_civil'] as $nat_civil_each)
        <option value="{{$nat_civil_each}}"
                @if(isset($event) ? $nat_civil_each == $event->nat_civil : false)
                    selected
                @endif
                >{{ $nat_civil_each }}</option>
    @endforeach
</select>

<label for="category">Category</label>
<select name="category" id="category" class="browser-default">
    <option value="" disabled selected>Select Category</option>
    @foreach($decodeArray['categories'] as $category)
        <option value="{{$category}}"
                @if(isset($event) ? $category == $event->category : false)
                    selected
                @endif
                >{{$category}}</option>
    @endforeach

</select>

<div class="input-field">
    <input type="number" step="0.1" id="issf_qualification_score"
           name="issf_qualification_score"
           value="{{ $event->issf_qualification_score or ""}}"
            >
    <label for="issf_qualification_score">ISSF Qualification Score</label>
</div>

<div class="input-field">
    <input type="number" id="nr_qualification_score"
           name="nr_qualification_score"
           value="{{ $event->nr_qualification_score or ""}}"
            >
    <label for="nr_qualification_score">NR Qualification Score</label>
</div>
<div class="input-field">
    <input type="number" name="fees" id="fees"
           value="{{ isset($event) ? $event->fees : ""}}"
            >
    <label for="fees">Fees</label>
</div>
<div class="input-field">
    <input type="number" name="max_score" id="max_score"
           value="{{ isset($event) ? $event->max_score : ""}}"
            >
    <label for="max_score">Max Score  TODO Determine if this is useful</label>
</div>


<input type="hidden" name="consider_for_qualification" id="consider_for_qualificationHidden" value="0" >
<div class="input-field">
    <input type="checkbox" id="consider_for_qualification" name="consider_for_qualification"
           value="1"
           @if(isset($event) ? $event->consider_for_qualification == 1 : false)
           checked
            @endif
            >
    <label for="consider_for_qualification"> Consider score for Qualification</label>
</div>
<div class="input-field">
    <input type="submit" value="{{$SubmitButtonText}}" class="btn btn-primary">
</div>

@include('errors.list')