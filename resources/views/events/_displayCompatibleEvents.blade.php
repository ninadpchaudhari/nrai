<h4>Please select the events you desire to participate</h4>
<div class="col s12 ">

    <ul class="collapsible" data-collapsible="accordion">
        @foreach($classes as $classKey => $class)
            <li>
                <div class="collapsible-header">
                    <i class="material-icons">filter_{{$classKey + 1}}</i>{{$class}}
                </div>
                <div class="collapsible-body" style="padding:1em">
                    <div class="row">
                        <div class="col s12">
                            <table class="striped" >
                                <thead>
                                <tr>
                                    <th data-field="select">Select</th>
                                    <th data-field="name">Event Name</th>
                                    <th data-field="desc">Description</th>
                                    <th data-field="issf_qualification_score">min ISSF Score</th>
                                    <th data-field="nr_qualification_score">min NR Score</th>
                                    <th data-field="fees">Fees</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($playableEvents as $event)
                                    @if($event->class == $class)


                                        <tr>
                                            <td><input type="checkbox" id="{{$event->id}}" name="{{$event->id}}" data-eventname="{{$event->name}}">
                                                <label for="{{$event->id}}"></label>
                                            </td>
                                            <td>{{$event->name}}</td>
                                            <td>
                                                {{--$event->class.' '--}}
                                                {{$event->type . ' '}}
                                                {{$event->nat_civil.' '}}
                                                {{$event->category.' '}}
                                            </td>
                                            <td>{{ $event->issf_qualification_score }}</td>
                                            <td>{{ $event->nr_qualification_score }}</td>
                                            <td> {{$event->fees}} </td>
                                        </tr>


                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </li>
        @endforeach


    </ul>




</div>