@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Information</div>
                    <div class="panel-body">
                        <div class="list-group">
                            <div class="list-group-item">{{$unit->name}}</div>
                            <div class="list-group-item">Total Ammount payable to NRAI : {{$grandTotal_liability}}</div>

                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Payment Due</div>
                    <div class="panel-body">
                        <table class="table table-striped">
                            <thead>
                            <th>Competition</th>
                            <th>Total Entries</th>
                            <th>Total Liabilities</th>
                            </thead>
                            <tbody>
                            @foreach($matches as $match)
                                <tr>
                                    <td>{{$match->short_name." " .$match->year.",". $match->place}}</td>
                                    <td>{{$match->total_forms}}</td>
                                    <td>{{$match->total_liability}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <div class="panel panel-default">
                    <div class="panel-heading">Shooter Accept/Reject panel</div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-hover" id="myTable">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>ShooterID</th>
                                        <th>Mobile No.</th>
                                        <th>Events</th>
                                        <th>Amount</th>
                                        <th>Payment</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($forms as $form)
                                        <tr>
                                            <td id="formID">{{$form->id}}</td>
                                            <td>{{$form->athlete->shooterName}}</td>
                                            <td>{{$form->shooterID}}</td>
                                            <td>{{$form->mobileNo}}</td>
                                            <td>{{implode(",",$form->events_array)}}</td>
                                            <td>{{$form->amount}}</td>
                                            <td>
                                                @if($form->payment_option == 1 && $form->payment_evidence == "")

                                                <input id="receiptID" type="text" class="form-control" placeholder="Enter Receipt">



                                                @else
                                                    {{$form->payment_evidence}}
                                                @endif
                                            </td>

                                            <td>
                                            <button class="rejectButton btn btn-danger">
                                                    <span > Reject</span>
                                                </button>
                                                @if($form->verified_by != 1)
                                                   
                                                <button  class="receiptBtn btn btn-default">Accept</button>
                                                
                                                @else
                                                
                                                
                                                        
                                                    @endif




                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer')
<script src="http://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script>
    $(document).ready(function(){
    $('#myTable').DataTable();
});
        $(".receiptBtn").on('click',function (event) {
            event.preventDefault();
            //alert("receiptForm clicked");
            var ID = $(this).closest('tr').children('td#formID').text();
            var receiptID = $(this).closest('tr').find('#receiptID').val();
            //alert(receiptID);
            $.ajax({
                type: 'PUT',
                url: '{{route('formEdit')}}',
                data: {
                    "_token": "{{csrf_token()}}",
                    "id": ID,
                    "receipt_id": receiptID
                },
                success: function (data) {
                    //alert("success" + data);
                    location.reload();
                    console.log(data);
                },
                error: function (data) {
                    alert('failed'+ data);
                    console.log(data.responseText);
                }

            });
        });

        $(".rejectButton").on('click',function () {
            var ID = $(this).closest('tr').children('td#formID').text();
            //alert("Reject button clicked : "+ ID);
            $.ajax({
                type: 'DELETE',
                url: '{{route('formReject')}}',
                data: {
                    "_token": "{{csrf_token()}}",
                    "id": ID
                },
                success: function (data) {
                    alert("success" + data);
                },
                error: function (data) {
                    alert('failed'+ data);
                    console.log(data.responseText);
                }

            });
        });

    </script>
@endsection