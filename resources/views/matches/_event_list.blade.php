

<table>
    <thead>
    <tr>
        <th data-field="name">Name</th>
        <th data-field="class">Class</th>
        <th data-field="type">Information</th>
        <th data-field="issf_qualification_score">min ISSF Score</th>
        <th data-field="nr_qualification_score">min NR Score</th>
        <th data-field="fees">Fees</th>
    </tr>
    </thead>
    <tbody>
    @foreach($events as $event)
        <tr>
            <td>{{ strtoupper($event->name) }}</td>
            <td>{{ucwords($event->class) }}</td>
            <td>{{ strtoupper($event->type).' ' }}
                {{ $event->gender.' ' }}
                {{ $event->nat.' ' }}
                {{ $event->category }}
            </td>
            <td>{{ $event->issf_qualification_score }}</td>
            <td>{{ $event->nr_qualification_score }}</td>
            <td>{{ $event->fees }}</td>
            <td>
                    <button class=" btn-flat lighten-1"
                            type="submit"
                            name="action"
                            onclick="location.href = '{{ action('eventsController@edit',[$event->id]) }}'"
                            >
                        <i class="material-icons">launch</i>
                    </button>


            </td>
        </tr>
        @endforeach
    </tbody>
</table>