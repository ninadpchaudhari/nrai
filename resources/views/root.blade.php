@extends('app')
@section('header')
    <meta name="google-signin-client_id" content="73565556138-akboop34c8atml3oteioo947tlqvt820.apps.googleusercontent.com">
    <script src="https://apis.google.com/js/platform.js" async defer></script>
@endsection

@section('content')
    {{--
    Hidding all the stuff for client side login
    <div class="container">
        <div class="valign-wrapper">
            <div class="g-signin2 valign" data-onsuccess="onSignIn"></div>

        </div>
    </div>
       --}}
    <a href="{{route('authRedirect',['google'])}}" class="btn btn-primary">Login with Google</a>
    @endsection
@section('footer')
    {{--
    <script>
        function onSignIn(googleUser) {
            var profile = googleUser.getBasicProfile();
            console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
            console.log('Name: ' + profile.getName());
            console.log('Image URL: ' + profile.getImageUrl());
            console.log('Email: ' + profile.getEmail());
        }
    </script>
    --}}
    @endsection