@extends('app')
@section('content')
<h4>Verification</h4>
    <p>Hi, {{$athlete->shooterName}} representing {{$athlete->stateOfRep}}</p>
    <p>Shooter ID : {{$athlete->shooterID}}</p>
    <p>Form ID : {{$form->id}}</p>
    <p>You have received a SMS from NRAI with One Time Password on</p>
    <p>Mobile No.: {{ $form->mobileNo }} ; Please Enter the same below </p>
    {!! Form::open(['route'=>'verifyFormOTP','method'=>'POST']) !!}

    <div class="row">
        <div class="col s4 input-group">
            {!! Form::label('clientOTP') !!}
            {!! Form::number('clientOTP',null,['id'=>'clientOTP','length'=>'6']) !!}
        </div>
        <div class="col s2 input-field">
            {!! Form::submit('Verify',['class'=>'btn btn-primary cyan']) !!}
        </div>
    </div>

    {!! Form::close() !!}
    @endsection
@section('footer')
    <script>
        console.log({{ $serverOTP }});
    </script>
    @endsection