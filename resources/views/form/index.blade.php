@extends('app')
@section('header')
    {{-- For laravel's csrf_protection --}}
    <meta name="_token" content="{{ app('Illuminate\Encryption\Encrypter')->encrypt(csrf_token()) }}" />


@endsection
@section('content')


    <div class="row">
        <h5>Select the Championship</h5>
        <div class="col s12 input-field">
            <label for="match_id"></label>
            <select id="match_id" class="browser-default">
                @foreach($matches as $match)
                    <option value="default" disabled selected>Select the Match</option>
                    <option value="{{ $match->id }}">{{ $match->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <hr>
    <br>
    <div class="row">
        <div class="col s6">
            <h5>Please provide your NRAI ID</h5>

            <div class="input-field col s10">
                <label for="shooterID">Shooter's NRAI ID Card</label>
                <input type="text" class="validate" length="13" id="shooterID">
                <a href="#findShooterID" class="modal-trigger" id="findShooterIDLink">Don't remember your ID?</a>
            </div>
            <!--
            <button class="input-field col s1 btn waves-effect btn-primary"><i class="large material-icons">play_arrow</i>
            </button>
            -->
        </div>


    </div>
    <div id="findShooterID" class="modal">
    </div>
    <br>
    <div class="row">
        <hr>
        <h5 id="welcomeText">
        </h5>
    </div>

    <div class="row" id="displayEvents">
    </div>
    <div class="row" id="contactInfo">
        {!! Form::open(['route' => 'verifyForm','id'=>'formSubmit']) !!}
        {!! csrf_field() !!}
        {!! Form::hidden("match_id") !!}
        {!! Form::hidden("shooterID") !!}
        {!! Form::hidden("events") !!}
        {!! Form::hidden("eventNames") !!}
        {{-- Form::hidden("representing_unit") --}}
        <h4>Contact Information</h4>
        <p>Please provide your contact information for verification</p>
        <p>Please note : Mobile Number will be verified</p>
        <div class="input-field col s6">
            <i class="material-icons prefix">phone</i>
            <label for="mobileNo">Mobile Number:</label>
 {{----}}    <input type="text" class="validate" length="10" id="mobileNo" name="mobileNo">
        </div>
        <div class="input-field col s6">
            <div class="g-signin2" data-onsuccess="onSignIn"></div>
            <i class="material-icons prefix">email</i>
            <label for="email" data-error="Wrong Email ID">Email ID : (Optional)</label>
 {{----}}    <input type="email" class="validate" id="email" name="email">
        </div>
        <div class="row">
            <div class="col s12">
                {!! Form::submit('Verify',['class'=>'btn btn-primary form-control']) !!}
                {!! Form::close() !!}
            </div>

        </div>
    </div>

    <div class="row">

    </div>
    @endsection
@include('errors.list')
@section('footer')
    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-XSRF-Token': $('meta[name="_token"]').attr('content')
                }
            });
        });
    </script>
    <script src="js/form.js"></script>

    @endsection