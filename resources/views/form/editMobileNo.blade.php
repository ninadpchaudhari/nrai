@extends('app')
@section('content')
<h4>Verification</h4>
    <p>Hi, {{$athlete->shooterName}} representing {{$athlete->stateOfRep}}</p>
    <p>Shooter ID : {{$athlete->shooterID}}</p>
        <p>There was some error[ {{ $error }} ] in sending message to your Mobile No.:{{$mobileNo}}</p>

        <p>Please Re-Enter your mobile no.</p>
        {!! Form::open(['route'=>'verifyForm','method'=>'PUT']) !!}
        <div class="row">
            <div class="col s4 input-group">
                {!! Form::label('mobileNo') !!}
                {!! Form::number('mobileNo',null,['id'=>'mobileNo','length'=>'10']) !!}
            </div>
            <div class="col s2 input-field">
                {!! Form::submit('Send OTP',['class'=>'btn btn-primary cyan']) !!}
            </div>
        </div>

        {!! Form::close() !!}

    @endsection
@section('footer')
    <script>
        console.log({{ $serverOTP }});
    </script>
    @endsection