<div class="modal-content">
    <h4>Find Your Nrai ID</h4>

    <form action="{{route('APISearchShooters')}}" method="POST" id="findForm">
        {!! csrf_field() !!}
        <div class="row">
            <div class="col s8 ">
                <div class="input-field">
                    <label for="findName">Start typing your Name to begin search</label>
                    <input type="text" name="findName" id="findName" autocomplete="off">
                </div>


            </div>
            <div class="col s4">
                <a href="javascript:void(0)" id="showFilters" class="btn btn-flat waves-effect waves-teal">
                    Add Filters <i class="material-icons right">filter_list</i>
                </a>
            </div>
        </div>
        <div id="filters">
            <div class="row">
                <div class="col s12 ">

                    <select id="findUnit" name="findUnit" class="browser-default">

                        <option value="%" disabled selected>Select the Unit</option>
                        @foreach($units as $unit)
                            <option value="{{ $unit->id }}">{{ $unit->name }}</option>
                        @endforeach
                    </select>
                </div>
        </div>

            <div class="row">
                <div class=" col s6 input-field">
                    <label for="findGender"></label>
                    <select name="findGender" id="findGender" class="browser-default">
                        <option value="%" disabled selected>Select the Gender</option>
                        @foreach($genders as $gender)
                            <option value="{{ $gender}}">{{ $gender}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col s6 input-field">
                    <label for="findYear">Enter Your Birth Year(Optional)</label>
                    <input type="number" id="findYear" name="findYear" max="{{date("Y")}}" maxlength="4" value="">
                </div>

            </div>

            <input type="submit" class="btn btn-default" value="Search">
    </div>

    </form>
    <div class="row" id="findResults">
    </div>

    <div id="template-list">
        <div class="row">
            <div class="col s2">
                <img src="{shooterPhoto}" alt="Athlete Image" class="">
            </div>
            <div class="col s8 offset-s1">
                <h5>{shooterID}</h5>
                <span>
                    <p>Name : {shooterName}</p>
                    <p>Unit of Representation : {stateOfRep}</p>
                </span>

            </div>
        </div>
    </div>
    <div id="template-card">
        <div class="col s6 card">
            <div class="card-image waves-effect waves-block waves-light">
                <img src="{shooterPhoto}" alt="Athlete Image" class="activator">
            </div>
            <div class="card-content">
                    <span class="card-title activator grey-text text-darken-4">
                        {shooterID}<i class="material-icons right">more_vert</i>
                    </span>
            </div>
            <div class="card-reveal">
                <span class="card-title activator grey-text text-darken-4">
                        {shooterID}<i class="material-icons right">more_vert</i>
                </span>
                <p>Name : {shooterName}</p>
                <p>Gender : {sex}</p>
                <p>Birth Year : {YOB}</p>
                <p>State : {state}</p>
                <p>Unit of representation  : {stateOfRep}</p>
            </div>
        </div>
    </div>




</div>
<div class="modal-footer">
    <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Close</a>
</div>


{{-- Have to remove this vendor after i am done with working on this partial --}}
    {{-- <script src="/js/vendor.js"></script> --}}
<script>
    $(document).ready(function(){
        $('#template-card').hide();
        $('#template-list').hide();
        $('#filters').hide();
        //$("#findUnit").selectize({
        //    create:true,
        //    sortField:'text'
        //});
        //$("#findGender").selectize();

    });
    $("#showFilters").on('click', function () {
        $('#filters').show();
    });
    $("#findForm").submit(function ( e ){
        e.stopImmediatePropagation();
        e.preventDefault();

        //var findName = $(this).find("input[name='findName']").val();
        //var findUnit = $("select#findUnit :selected").val();
        //var findGender = $(this).find("select#findGender :selected").val();
        //var findYear = $(this).find("input[name='findYear']").val();

        //if(findUnit == 'default' || typeof findUnit === 'undefined'){
        //    console.log("findUnit Default or undefined");
        //    $("#findResults").html("Select Unit");
        //    return;
        //}
        //if(findGender == 'default' || typeof findUnit === 'undefined' ){
        //    console.log("findgender Default or undefined");
        //    $("#findResults").html("Select Gender");
        //    return;
        //}
        //if(findName.length < 3){
        //    $("#findResults").html("Name is less han 3 chars");
        //    return;
        //}
        //if (!typeof findYear === 'undefined') {
        //    if( findYear >= new Date().getFullYear() || findYear < 1900 ){
        //            console.log("FindYear out of bonds");
        //        $("#findResults").html("Year Out of Bonds");
        //        return;
        //    }
        //}


        var url = "{{ route('APISearchShooters') }}";
        var request = $.ajax({
            url: url,
            method: "POST",
            dataType: "json",
            data : $(this).serialize()
        });
        //todo Send ajax request only after some time...
        //todp see some 404 errors
        request.done(function (msg) {
            document.getElementById('findResults').innerHTML = "";
            var shooters = msg.data;
            $.each(shooters, function (index, shooter) {
                console.log(shooter);
                shooter.shooterPhoto = "/api/athletePhoto/" + shooter.shooterID;
                document.getElementById('findResults').innerHTML +=
                        nano($("#template-list").html(), shooter);
            });

        });

        request.fail(function (jqXHR, textStatus) {
            console.log(jqXHR.responseText);
            $("#findResults").html("Request Failed " + jqXHR.status + " " + jqXHR.statusText);
        });

    });
    $("#findName").on('keyup',function(){
        if($(this).val().length >= 3 ) $("#findForm").trigger('submit');
    });
    //Templating Engine :p LOL
    function nano(template, data) {
        return template.replace(/\{([\w\.]*)\}/g, function(str, key) {
            var keys = key.split("."), v = data[keys.shift()];
            for (var i = 0, l = keys.length; i < l; i++) v = v[keys[i]];
            return (typeof v !== "undefined" && v !== null) ? v : "";
        });
    }
</script>