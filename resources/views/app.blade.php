<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>NRAI Forms</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->
<style>
    .StripeElement {
        background-color: white;
        padding: 8px 12px;
        border-radius: 4px;
        border: 1px solid transparent;
        box-shadow: 0 1px 3px 0 #e6ebf1;
        -webkit-transition: box-shadow 150ms ease;
        transition: box-shadow 150ms ease;
    }

    .StripeElement--focus {
        box-shadow: 0 1px 3px 0 #cfd7df;
    }

    .StripeElement--invalid {
        border-color: #fa755a;
    }

    .StripeElement--webkit-autofill {
        background-color: #fefde5 !important;
    }
</style>


            <!-- Traditional-->
     <link rel="stylesheet" href="{{ asset(url('css/vendor.css')) }}">
    <link rel="stylesheet" href="{{ asset(url('css/selectize.css')) }}">
    <!-- Production -->
    {{-- <link rel="stylesheet" href="{{ elixir('css/app.css') }}"> --}}

     <link rel="stylesheet" href="{{ asset(url('css/app.css')) }}">

    {{--<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">--}}
    <link rel="stylesheet" href="/font/material-icons/material-icons.css">
    @yield('header')
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
@include('old_navigation')


<div class="container">
@yield('content')

</div>





<script src="/js/vendor.js"></script>
@yield('footer')
{{--<script src="js/app.js"></script>--}}
{{--
 When in Production add versioning
 <script src="{{elixir('js/app.js')}}"></script> --><!-- Google Analytics: change UA-XXXXX-X to be your site's ID.
 --}}
<!--
<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
    function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
    e=o.createElement(i);r=o.getElementsByTagName(i)[0];
    e.src='https://www.google-analytics.com/analytics.js';
    r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X','auto');ga('send','pageview');
</script>
-->
</body>
</html>