<?php
/**
 * Created by PhpStorm.
 * User: Ninad
 * Date: 15/10/2015
 * Time: 10:55 PM
 */

namespace App\Packages\SMSGateway;

use Log;

class SMS
{

    /**
     * Sends SMS Via Gupshup Enterprise
     * TESTED on 10/10/2015 Works
     *
     * On success it gives :
     * [0] => "success"
     * [1] => "919405441318" // "91" followed byMobile Number
     * [2] => "2893885356146180194-138688659500872106\n"
     *        //Probably the sms sent ID followed by '\n'
     * on error it gives
     * [0] => "error"
     * [1] => 175 // Error Code
     * [2] => Error Explanation in detail
     *
     * @param $send_to String Mobile Number of the person to send the SMS
     * @param $msg String The Message to be sent, Note : the Message must be in templates
     * @return true if success else error code
     */
    public function sendSMS($send_to, $msg)
    {
        $request =""; //initialise the request variable
        $param["method"]= "sendMessage";
        $param["send_to"] = $send_to;
        $param["msg"] = $msg;
        $param["userid"] = "2000144075";
        $param["password"] = "NINAD111";
        $param["v"] = "1.1";
        $param["msg_type"] = "TEXT"; //Can be "FLASH�/"UNICODE_TEXT"/�BINARY�
        $param["auth_scheme"] = "PLAIN";
//Have to URL encode the values
        foreach ($param as $key => $val) {
            $request.= $key."=".urlencode($val);
//we have to urlencode the values
            $request.= "&";
//append the ampersand (&) sign after each parameter/value pair
        }
        $request = substr($request, 0, strlen($request)-1);
//remove final (&) sign from the request
        $url =
            "http://enterprise.smsgupshup.com/GatewayAPI/rest?".$request;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);
        $return = explode(' | ', $curl_scraped_page);

        if ($return[0] == "success") {
            return "true"; // returns true if successful
        } else {
            return $return[1]; // Returns the Error code if some error
        }
    }
    /**
     * @param $mobileNo
     * @return int|true Return OTP if successful else return Error code
     */
    public function otp($mobileNo)
    {
        $otp = mt_rand(111111, 999999);
        $message =
            "Dear Athlete,\nThank you for using NRAI Online Services,\nYour OTP is : {$otp}";
        $response = $this->sendSMS($mobileNo, $message);
            Log::info($response);
            Log::info($otp);
        if ($response == true) {
            return $otp;
        } else {
            return $response;
        }
    }

    public function formPaymentCashRequested($form_id)
    {
        $form = \App\Form::findOrFail($form_id);
        $match = \App\Match::findOrFail($form->match_id);
        $message ="Dear Athlete,\nPayment for your Form ID: {$form_id} is due.\nPlease make the same at your state association before {$match->last_date}";
        //todo make schedule to reject forms every night based on last_date
        $response = $this->sendSMS($form->mobileNo, $message);
        return $response;
    }
    public function test_formPaymentCashRequested($form_id)
    {
        $form = \App\Form::findOrFail($form_id);
        $match = \App\Match::findOrFail($form->match_id);
        $message ="Dear Athlete,\nPayment for your Form ID: {$form_id} is due.\nPlease make the same at your state association before {$match->last_date}";
        $response = true;
        return $response;
    }
    public function onlinePaymentDone($form)
    {
        $message ="Dear Athlete,\nYour Form ID: {$form->id} is received,\nNote:Acceptance is subject to your state association's approval\nThank you for using NRAI Online Services";
        $response = $this->sendSMS($form->mobileNo, $message);
        return $response;
    }

    public function test()
    {
        return "test";
    }
}
