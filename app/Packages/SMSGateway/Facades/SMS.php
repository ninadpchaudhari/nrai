<?php
/**
 * Created by PhpStorm.
 * User: Ninad
 * Date: 15/10/2015
 * Time: 10:56 PM
 */

namespace App\Packages\SMSGateway\Facades;

use Illuminate\Support\Facades\Facade as BaseFacade;

class SMS extends BaseFacade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return '\App\Packages\SMSGateway\SMS'; }

}