<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
    //
    protected $guarded = [
        'series'
    ];
    protected $casts = [
        'shots' => 'array',
        'series' => 'array'
    ];
    public function athlete(){
        return $this->belongsTo('App\Athlete','shooterID','shooterID');
    }
    public function event(){
        return $this->belongsTo('App\Event');
    }

    public function finalScore()
    {
        return $this->hasMany('App\FinalScore');
    }
    public function setShotsAttribute($value){
        $tempSeries = [];
        foreach($value as $i=>$tenShots){
            if (is_numeric($tenShots)) {
                //Series isnt a array , but only 1 number .
                //Assign the series values and then continue with the loop
                $tempSeries[$i] = $tenShots;
            } else {
                //Do total and then save to Series array
                $tempSeries[$i] = array_sum($tenShots);
            }
        }
        $this->series = $tempSeries;
    }

}
