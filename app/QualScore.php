<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QualScore extends Model
{
    //
    protected $table = "qual_scores";

    /*
     * Returns ISSF Qualification score
     */
    public static function getQualScore($class,$type,$gender,$category)
    {
        $category = \App\QualScore::checkIfExistsElseGenralize($class,$type,$gender,$category)['category'];
        return \App\QualScore::where('class','=',$class)
            ->where('type','=',$type)
            ->where('gender','=',$gender)
            ->where('category','=',$category)
            ->value('qual_score');
    }

    /**
     * Gives the max_score for a event
     * If type not found, like eg FOREIGN NATIONALS , it falls back to ISSF
     * @param $class string
     * @param $type string
     * @param $gender string gender
     * @param $category string category
     * @return integer or decimal max_score
     */
    public static function MaxScore($class,$type,$gender,$category)
    {
        $type = \App\QualScore::checkIfExistsElseGenralize($class,$type,$gender,$category)['type'];
        return \App\QualScore::where('class','=',$class)
            ->where('type','=',$type)
            ->where('gender','=',$gender)
            ->where('category','=',$category)
            ->value('max_score');
    }

    public static function checkIfExistsElseGenralize($class,$type,$gender,$category)
    {
        $returnArray = array();
        //Return all types for the given class and gender
        $types = \App\QualScore::where('class','=',$class)
            ->where('gender','=',$gender)
            ->distinct()->lists('type')->toArray();

        if(!in_array($type,$types)) $type = 'ISSF';
        $returnArray['type'] = $type;
        $categories = \App\QualScore::where('class','=',$class)
            ->where('gender','=',$gender)
            ->distinct()->lists('category')->toArray();

        if(!in_array($category,$categories)) $category = 'Senior';
        $returnArray['category'] = $category;
        return $returnArray;
    }
}
