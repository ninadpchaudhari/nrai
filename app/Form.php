<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Cashier\Billable;
use Laravel\Cashier\Contracts\Billable as BillableContract;

class Form extends Model implements BillableContract
{
    //
    use Billable;

    protected $dates = ['trial_ends_at', 'subscription_ends_at'];
    public function match()
    {
        return $this->belongsTo('App\Match');
    }
    public static function decodePaymentOptions($payment_option){
        switch($payment_option){
            case 0:
                return "NOT DONE";
            break;
            case 1 :
                return "CASH";
                    break;
            case 2:
                return "DD";
                    break;
            case 3:
                return "ONLINE";
                    break;
        }
    }
    public function scopeRequireUnitVerification($query){
        return $query->whereRaw('verified_by < 2');
    }
    public function athlete(){
        return $this->hasOne('App\Athlete','shooterID','shooterID');
    }
    public function competition(){
        return $this->hasOne('App\Match','id','match_id');
    }
}
