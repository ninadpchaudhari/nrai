<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class formValidationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'match_id'=>'required|numeric|exists:matches,id',
            'shooterID'=>'required|exists:athletes,shooterID',
            'events'=>'required|string',
            'eventNames'=>'required|string',
            'mobileNo'=>'required|numeric|regex:/^\d{10}$/',
            'email'=>'email'
        ];
    }
}
