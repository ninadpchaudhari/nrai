<?php

namespace App\Http\Controllers;

use Mail;
use App\Packages\SMSGateway\Facades\SMS;
use App\Form;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Razorpay\Api\Api;

class paymentController extends Controller
{
    public function formOptions()
    {
        $form = Form::findOrFail(\Session::get('form_id'));
        return view('payments.formOptions', compact('form'));
    }

    public function formCash()
    {
        $form = \App\Form::findOrFail(\Session::get('form_id'));
        $unit = \App\Unit::findOrFail($form->representing_unit);
        $endDate = \App\Match::findOrFail($form->match_id)->first()->end_date;
        \Session::remove('form_id');
        $form->payment_option = 1;
        $form->save();
        SMS::formPaymentCashRequested($form->id);
        //todo verify mail first !
        //todo add special Email views ...
        //Mail::send('payments.formCash',compact('form','unit','endDate'), function ($m) use ($form) {
        //    $m->to($form->email, $form->shooterID)->subject('NRAI Form');
        //});
        //middle ware for human check
        //last date validation
        //last date displayed as 00:00:00
        return view('payments.formCash', compact('form', 'unit', 'endDate'));
    }

    public function formOnline()
    {
        $form = \App\Form::findOrFail(\Session::get('form_id'));
        $unit = \App\Unit::findOrFail($form->representing_unit);
        $endDate = \App\Match::findOrFail($form->match_id)->first()->end_date;

        return view('payments.formOnline', compact('form'));
    }
    public function catchOnlinePayment(Request $request)
    {
        //Payment is authorized.
        $form = \App\Form::findOrFail(\Session::get('form_id'));
        $api_key = env('RAZORPAY_API_KEY', '');
        $api_secret = env('RAZORPAY_API_SECRET', '');
        ;
        $razorApi = new Api($api_key, $api_secret);

        $captured_payment = $razorApi->payment
        ->fetch($request->razorpay_payment_id)
        ->capture(array('amount'=>($form->amount)*100));

        if ($captured_payment->id == $request->razorpay_payment_id) {
            \Session::remove('form_id');

            $form->payment_option = 2;
            $form->payment_evidence = $request->razorpay_payment_id;
            $form->save();
            SMS::onlinePaymentDone($form);
            return "Thank you for the Form{$form->shooterID}. *Acceptance subject to State's approval";
        }
        
        return "Payment Capture failed";
        ;
    }
}
