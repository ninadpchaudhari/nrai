<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Log;
use Auth;
use App\User;
use Illuminate\Support\Facades\Gate;
use Laravel\Socialite\Facades\Socialite;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    /**
     * Setting the Accepted & Configured Authentication
     * Service Providers in applications
     * @var array
     */
    private $authProviders = ['google','facebook'];
    protected $redirectPath = '/unit/showEntries';
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //Middleware wont be applicable for the auth routes now.
        //$this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        Log::notice("Adding User " . serialize($data));
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * Redirect the user to the Auth Provider Login page
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {

        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from Auth Provider.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        if(!in_array($provider,$this->authProviders))
        {
            return redirect()->back();
        }
        $user = Socialite::driver($provider)->user();
        dd($user);
        // For the CURL Error just set CURLOPT_SSL_VERIFYPEER to false ! in CurlFactory.php
        // $user->token;r
    }

    /**
     * Show the User registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        if (Gate::denies('register-users', [Auth::user(), New \App\User()])) {
            Log::warning(Auth::user()->name . " Tried to access showRegistrationForm");
            abort(403, "You are not allowed to register Users");
        }

        if (property_exists($this, 'registerView')) {
            return view($this->registerView);
        }

        return view('auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        if (Gate::denies('register-users', [Auth::user(), New \App\User()])) {
            Log::warning(Auth::user()->name . " Tried to register a new user");
            abort(403, "You are not allowed to register Users");
        }
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $this->create($request->all());

        return redirect($this->redirectPath());
    }
}
