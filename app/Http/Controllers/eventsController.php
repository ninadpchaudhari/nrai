<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
//use DebugBar\DebugBar;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class eventsController extends Controller
{

    public function index(){
        $events = \App\Event::all();
        return view('matches._event_list',compact('events'));
    }
    public function matchIndex($match_id)
    {
        //
        $match = \App\Match::findOrFail($match_id);
        $events = \App\Event::find($match_id);
        return view('events.matchIndex', compact('match', 'events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        //
        $match_id = $request->match_id;
        $decodeArray = \App\Event::decodeArray();

        //app('debugbar')->info($match_id);

        $match = \App\Match::findOrFail($match_id);
        return view('events.create', compact('match_id', 'match', 'decodeArray'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Requests\eventsRequest $request)
    {
        //
        app('debugbar')->info($request->all());

        \App\Event::create($request->all());

        return redirect(action('matchesController@show', [$request->get('match_id')]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $event = \App\Event::findOrFail($id);
        $decodeArray = \App\Event::decodeArray();

        return view('events.edit',compact('event','decodeArray'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return Response
     */
    public function update( $id,Requests\eventsRequest $request)
    {
        //
        $event = \App\Event::findOrFail($id);
        $event->update($request->all());

        $event->save();

        return redirect()->action('matchesController@show',[$event->match_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($event_id)
    {
        //
        app('debugbar')->info('Destroying' . $event_id);
        \App\Event::destroy($event_id);
        return Redirect::back();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    // Gives types and classes which player has played match earlier in 2 years
    public function getPlayableClassType($shooterID,$matchYear){
        $playableArray['types'] = Array();
        $playableArray['classes'] = Array();
        $minYear = $matchYear - 2;
        $eventTypeClass=
            DB::table('scores')
                ->select('events.type','events.class')
                ->join('events', 'events.id','=','scores.event_id')
                ->join('matches',function($join) use ($minYear) {
                    $join->on('events.match_id','=','matches.id')
                        ->where('matches.year','>=',$minYear);
                })
                ->where(['scores.shooterID' => $shooterID])
                ->distinct()
                ->get();

        foreach($eventTypeClass as $eventTypeClass_1) {
            $playableArray['types'][] = $eventTypeClass_1->type;
            $playableArray['classes'][] = $eventTypeClass_1->class;
        }

        return $playableArray;
    }

    /**
     * @param $shooterID
     * @param $matchYear
     * @return Array of Playable Classes
     */
    public function getPlayableClass($shooterID,$matchYear){

        $minYear = $matchYear - 2;

        $classes=
            DB::table('scores')
                ->join('events', 'events.id','=','scores.event_id')
                ->join('matches',function($join) use ($minYear) {
                    $join->on('events.match_id','=','matches.id')
                        ->where('matches.year','>=',$minYear);
                })
                ->where(['scores.shooterID' => $shooterID])
                ->distinct()
                ->lists('events.class');
        return $classes;
    }
    public function getPlayableGender($athleteSex){
        $playableArray = Array();
        //Common events can always be played
        $playableArray[] = 'Common';

        if(strcmp($athleteSex,'MEN') == 0){
            $playableArray[] = 'Men';
        }
        elseif(strcmp($athleteSex,'WOMEN') == 0){
            $playableArray[] = 'Women';
        }
        else dd('Error in getting PlayableGender'.$playableArray);
        return $playableArray;
    }
    public function getPlayableCategories($athlete,$matchYear){

        $age = abs($matchYear - $athlete->DOB->year) ;
        if($age > 150 || $age < 10)
        {
            echo "Age Out of Bounds -- from Controller";
            dd($age);
        }
        $playableArray['categories'] = \App\Event::decodeArray()['categories'];

        if($age > 21) {
            //Senior Category Player
            return array_diff($playableArray['categories'],array('Junior','Youth'));
        }elseif($age < 21 && $age >18){
            //Junior Category Player
            return array_diff($playableArray['categories'],array('Youth'));
        }elseif($age<18){
            //Youth Category Player
            return $playableArray['categories']; // no change
        }

    }
    public function getMaxScore($matchYear,$shooterID,$class,$type){
        $minYear = $matchYear - 2;
        $maxScore = DB::table('scores')
            ->join('events',function($join) use($class,$type) {
                $join->on('events.id','=','scores.event_id')
                    ->where('events.consider_for_qualification','=',1)
                    ->where('events.class','=',$class)
                    ->where('events.type','=',$type);
            })
            ->whereIn('events.match_id',function($query)use($minYear){
                $query->select('id')
                    ->from('matches')
                    ->where('year','>=',$minYear);
            })


            ->where('scores.shooterID',$shooterID)

            ->max('scores.score');
        ;

        return $maxScore;

    }
    public function isQualified($matchYear,$shooterID,$compatibleEvent){
        if( $this->getMaxScore($matchYear,$shooterID,$compatibleEvent->class,'ISSF')
            >= $compatibleEvent->issf_qualification_score ||
            $this->getMaxScore($matchYear,$shooterID,$compatibleEvent->class,'NR')
            >= $compatibleEvent->nr_qualification_score
        ) return true;
        else return false;
    }

    /**
     * @param $match
     * @param $athlete
     * @return all the classes,types,etc etc that the athlete can play based on himself
     */
    public function getPlayableProfile($match,$athlete){
        $playableProfile = Array();
        $playableProfile['classes'] = $this->getPlayableClass($athlete->shooterID,$match->year); // as 2 keys, the function itself returns keys value pairs
        $playableProfile['types'] = \App\Event::decodeArrayForMatch_id($match->id,"types");
        $playableProfile['categories'] = $this->getPlayableCategories($athlete,$match->year);
        $playableProfile['genders'] = $this->getPlayableGender($athlete->sex);
        $playableProfile['nat_civil'] = \App\Event::decodeArrayForMatch_id($match->id,'nat_civil');
        return $playableProfile;
    }

    /**
     * Accepts $playableProfile as  From the getPlayableProfile function
     * @param $match_id
     * @param $playableProfile
     * @return Find all the events that match the athletes profile
     */
    public function getCompatibleEvents($match_id,$playableProfile){
        $playableEvents= Array();
        $playableEvents = DB::table('events')
            ->select('events.*')
            ->where('match_id','=',$match_id)
            ->whereIn('class',$playableProfile['classes'])
            ->whereIn('type',$playableProfile['types'])
            ->whereIn('gender',$playableProfile['genders'])
            ->whereIn('nat_civil',$playableProfile['nat_civil'])
            ->whereIn('category',$playableProfile['categories'])
            ->get();
        return $playableEvents;

    }
    public function athleteCompatibleEvents($match_id, $shooterID)
    {
        $shooterID = (string) $shooterID;
        $athlete = \App\Athlete::where('shooterID', $shooterID)->first();
        $match = \App\Match::findOrFail($match_id);

        //Get all the classes,types,etc etc that the athlete can play based on himself
        $playableProfile = $this->getPlayableProfile($match,$athlete);

        // Find all the events that match the athletes profile
        $playableEvents = $this->getCompatibleEvents($match_id,$playableProfile);
        //dd($playableProfile);

        //Filter the events that the athlete is qualified for by score
        foreach($playableEvents as $playableEvent){
            if(! $this->isQualified($match->year,$shooterID,$playableEvent))
                unset($playableEvent);

        }
        return $playableEvents;

    }

    public function athleteCompatibleEventsPartial($match_id, $shooterID){
        $playableEvents = $this->athleteCompatibleEvents($match_id, $shooterID);
        $classes = Array();
        foreach($playableEvents as $playableEvent){
            if(in_array($playableEvent->class,$classes)){
                continue;
            }
            else{
                $classes[] = $playableEvent->class;
            }
        }
        return view('events._displayCompatibleEvents',compact('playableEvents','classes'));
    }

}
