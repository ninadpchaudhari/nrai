<?php

namespace App\Http\Controllers;

use App\Unit;
use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use DB;

class UnitController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    //
    public function index()
    {
        $user = Auth::user();
        $matches = \App\Match::all();
        $grandTotal_liability=0;
        if ($user->id == 1) {
            return redirect(route("showTotalEntries"));
        } else {
            //TODO: change this...
            $unit = \App\Unit::findOrFail($user->id -1);

            $forms = \App\Form::where(['representing_unit'=> $unit->id])
                ->requireUnitVerification()
                ->get();

            foreach ($matches as $match) {
                $match->load("forms");
                $match->total_forms = intval(DB::table('forms')
                                            ->where('representing_unit', $unit->id)
                                            ->where('match_id', $match->id)
                                            ->count('id'));
                $match->total_liability = floatval(DB::table('forms')
                                            ->where('representing_unit', $unit->id)
                                            ->where('match_id', $match->id)
                                            ->where('verified_by', '1')
                                            ->sum('amount'));
                $grandTotal_liability += $match->total_liability;
            }
        }
        $forms = $forms->load('athlete');
        $allEvents= \App\Event::all();

        foreach ($forms as $form) {
            $events = $form->events;
            $events = explode(",", $events);
            $events_array = [];
            foreach ($events as $event) {
                array_push($events_array, \App\Event::findOrFail($event)->name);
            }
            $form->events_array = $events_array;
        }
        

        //$forms = $forms->load('competition');

        //return compact('forms','unit','matches','grandTotal_liability');
        return view('unit.showEntries', compact('forms', 'unit', 'matches', 'grandTotal_liability'));
    }
    public function showTotalEntries()
    {
        $matches = \App\Match::all();
        $units = \App\Unit::all();
        //foreach($units as $unit){
        //    $unit['total_liability'] = floatval(DB::table('forms')
        //                                ->where('representing_unit',$unit->id)
        //                                ->where('match_id',$match_id)
        //                                ->sum('amount'));
        //}
    }
}
