<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use \App\Packages\SMSGateway\Facades\SMS;

class formController extends Controller
{
    public function index()
    {
        //
        

        $matches = \App\Match::where('last_date', '>=', \Carbon\Carbon::now())->get();

        return view('form.index', compact('matches'));
    }

    /**
     * Requires form_id from session
     * Generates OTP and
     * Displays Form for filling OTP
     * @return \Illuminate\View\View
     */
    public function verifyFormShowOTP()
    {
        if (!\Session::has('form_id')) {
            return Redirect::back();
        }
        $form_id = \Session::get('form_id');

        $form = \App\Form::findOrFail($form_id);
        $athlete = \App\Athlete::where('shooterID', $form->shooterID)->first();

        $mobileNo = \Session::pull('mobileNo');
        //$serverOTP = SMS::otp($mobileNo);
        $serverOTP = mt_rand(111111, 999999);
        \Session::set('serverOTP', $serverOTP);

        return view("form.getOTP", compact('athlete', 'form', 'serverOTP'));
    }

    /**
     * Needs serverOTP in session and clientOTP in request
     * It checks if OTP are same
     * @param Request $request POst request
     * @return string
     */
    public function verifyFormOTP(Request $request)
    {
        if (!isset($request->clientOTP)) {
            Log::notice("No Client OTP Provided in verifyFormOTP");
            return Redirect::back();
        }
        if (! \Session::has('serverOTP')) {
            \Log::notice("server OTP not found in Session at verifyFormOTP clientOTP : ".$request->clientOTP);
            return Redirect::route('formRoot');
        }
        $serverOTP = \Session::pull('serverOTP');

        if ($serverOTP != $request->clientOTP) {
            return "Wrong OTP";
        }

        $form_id = Session::get('form_id');
        $form = \App\Form::findOrFail($form_id);
        $form-> mobileNo_verified = 1;
        $form->save();
        return redirect(route('formPaymentOptions'));
    }



    public function totalFees($events)
    {
        $total = 0;
        $eventsArray = explode(',', $events);
        foreach ($eventsArray as $event) {
            $total += \App\Event::findOrFail($event)->fees;
        }
        return $total;
    }


    public function verifyForm(Requests\formValidationRequest $request)
    {

        $total = $this->totalFees($request->events);
        $form = new \App\Form;
        $form->amount = $total;
        $form->shooterID = $request->shooterID;
        if (isset($request->representing_unit)) {
            $form->representing_unit = $request->representing_unit;
        } else {
            $stateOfRep = \App\Athlete::where('shooterID', $request->shooterID)->first()->stateOfRep;
            $form->representing_unit = \App\Unit::where('name', strtolower($stateOfRep))->first()->id;
        }
        $form->match_id = $request->match_id;
        $match = \App\Match::findOrFail($form->match_id);
        if ($match->stateVerificationRequired) {
            $form->verified_by = 0;
        } else {
            $form->verified_by = 2;
        }
        $form->events = $request->events;
        $form->mobileNo = $request->mobileNo;
        $form->clientIP = \Request::getClientIp();
        if (isset($request->email)) {
            $form->email = $request->email;
        }
        $form->save();
        \Session::set('form_id', $form->id);
        \Session::set('mobileNo', $form->mobileNo);
        return redirect(route('verifyFormShowOTP'));
    }

    public function findShooterID(Request $request)
    {
        $units = \App\Unit::all();
        $genders = \App\Event::decodeArray()['genders'];

        return view('form._findShooterID', compact('units', 'genders'));
    }

    public function returnShooters(Request $request)
    {
        //findUnit
        //findName
        //findYear
        //findGender
        //dd($request->all());
        if (!isset($request->findUnit)) {
            $request->findUnit = '%';
        }
        if (!isset($request->findGender)) {
            $request->findGender = '%';
        }
        if (!isset($request->findYear)) {
            $request->findYear = '';
        }

        $shooters = \App\Athlete::where('representing_unit', 'like', $request->findUnit)
        ->where('shooterName', 'like', '%'.$request->findName.'%')
            ->where('sex', 'like', strtoupper($request->findGender))
        //->whereRaw("DATE_FORMAT(`DOB`,'%Y') = {$request->findYear}%")
            ->where('DOB', 'like', $request->findYear . '%')

            ->selectPublicInfo()
            ->paginate();
        //if(isset($request->findYear)){
        //    foreach($shooters as $shooter){
        //        if(\Carbon\Carbon::parse($shooter->DOB)->year != $request->findYear)
        //            unset($shooter);
        //    }
        //}
        return response()->json($shooters);
    }
    /*
     * Rejects the Form
     */
    public function reject(Request $request)
    {
        //if(! Auth::user()) return response("Not loggedin");
        $form = \App\Form::findOrFail($request->id);
        $form->delete();
        SMS::sendSMS($form->mobileNo, "Dear Athlete, Your form has been REJECTED by the State. Please contact the Association");

        return response("success", 200);
    }
    public function addReceipt(Request $request)
    {
        $form= \App\Form::findOrFail($request->id);
        if ($request->receipt_id == "") {
            return response('Receipt ID not set', 500);
        }
        $form->payment_evidence = $request->receipt_id;
        $form->verified_by = 1;
        $form->save();
        SMS::sendSMS($form->mobileNo, "Dear Athlete, Your form has been accepted by the State. Receipt ID: ".$form->payment_evidence);
        return response("success", 200);
    }
}
