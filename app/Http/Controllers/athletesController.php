<?php

namespace App\Http\Controllers;

use App\Athlete;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class athletesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($shooterID)
    {
        //
        //$shooter = Athlete::where('shooterID',$shooterID)->firstPublicInfo();
        $shooter = Athlete::where('shooterID',$shooterID)->selectPublicInfo()->first();
        if($shooter->exists())
        {
            /*
             * Adding YOB to public info
             */
            $DOB = \Carbon\Carbon::parse($shooter->DOB);
            unset($shooter->DOB);
            $shooter->YOB = $DOB->year;
            return $shooter;
        }
        else  return \Response::json(); //Dont fucking change this
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
