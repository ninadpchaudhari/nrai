<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/welcome', function () {
 //   return view('welcome');
//});

Route::get('/', ['as'=>'root','uses'=>'rootController@index']);
Route::resource('/api/athletePhoto', 'athletesPhotoController');
Route::resource('/api/athleteInfo', 'athletesController');
Route::get('/api/athleteCompatibleEvents/{match_id}/{shooterID}', 'eventsController@athleteCompatibleEvents');
Route::get('/partials/athleteCompatibleEvents/{match_id}/{shooterID}', 'eventsController@athleteCompatibleEventsPartial');
Route::get('/partials/findShooters', 'formController@findShooterID');
Route::post('/api/searchShooters', [
    'uses'=>'formController@returnShooters',
    'as' => 'APISearchShooters'
]);

Route::get('/form', ['as'=>'formRoot','uses'=>'formController@index']);
Route::post('/form/verify', ['as'=>'verifyForm','uses'=>'formController@verifyForm']);
Route::get('/form/verify/editMobile/{error_id}', ['as'=>'verifyFormEditMobile','uses'=>'formController@editMobileNo']);
Route::put('/form/verify/editMobile', ['as'=>'verifyFormUpdateMobileNo','uses'=>'formController@updateMobileNo']);
Route::get('/form/verifyOTP', ['as'=>'verifyFormShowOTP','uses'=>'formController@verifyFormShowOTP']);
Route::post('/form/verifyOTP', [
    'as'=>'verifyFormOTP',
    'uses'=>'formController@verifyFormOTP',
    'middleware' => 'formIdRequired'
]);
Route::delete('/form', ['as'=>'formReject','uses'=>'formController@reject']);
Route::put('/form', ['as'=>'formEdit','uses'=>'formController@addReceipt']);
Route::get('/payment/form', ['as'=>'formPaymentOptions','uses'=>'paymentController@formOptions','middleware'=>'formIdRequired']);
Route::get('/payment/form/cash', ['as'=>'formPaymentCash','uses'=>'paymentController@formCash','middleware'=>'formIdRequired']);
Route::get('/payment/form/online', ['as'=>'formPaymentOnline','uses'=>'paymentController@formOnline','middleware'=>'formIdRequired']);
Route::post('/payment/form/catch', ['as'=>'catchOnlinePayment','uses'=>'paymentController@catchOnlinePayment','middleware'=>'formIdRequired']);

// Socialite Routes
Route::get('auth/service/{provider}', [
    'as'=>'authRedirect',
    'uses'=>'Auth\AuthController@redirectToProvider']);
Route::get('auth/service/callback/{provider}', [
    'as'=>'authCallback',
    'uses'=>'Auth\AuthController@handleProviderCallback']);

Route::resource('matches', 'matchesController');

Route::get('events/match/{match_id}', 'eventsController@matchIndex');
Route::resource('events', 'eventsController');

//Added from MRA App
// --start-- Scores
Route::get('scores/match_id/{match_id}', ['as' => 'indexForSelectingClass', 'uses' => 'scoresController@indexForSelectingClass']);
Route::get('scores/match_id/{match_id}/class_id/{class_id}', ['as' => 'getScoresByClass', 'uses' => 'scoresController@indexForMatchAndClass']);
Route::post('scores/match_id/{match_id}/class_id/{class_id}', ['as' => 'storeScoresByClass', 'uses' => 'scoresController@storeForMatchAndClass']);
Route::get('scores/match_id/{match_id}/class_id/{class_id}/gender/{gender}', ['as' => 'getScoresByClassAndGender', 'uses' => 'scoresController@indexForMatchAndClassAndGender']);

Route::get('scores/match_id/{match_id}/class_id/{class_id}/gender/{gender}/category/{category}',
    ['as' => 'getScoresByClassAndGenderAndCategory',
        'uses' => 'scoresController@indexForMatchAndClassAndGenderAndCategory']);
// --end-- Scores

// -- start Ranking --
Route::get('ranks/match_id/{match_id}/print', ['as' => 'printRankingIndex', 'uses' => 'rankController@printIndex']);
Route::get('ranks/match_id/{match_id}/event_id/{event_name}', ['as' => 'printRankingByEventName', 'uses' => 'rankController@printByEventName']);
// -- end Ranking --

//Route::resource('events','eventsController');
/*
Route::group(['prefix' => 'admin'],function(){
    Route::get('/',function(){
        return view('admin.home');
    });
});

*/

Route::get('ip', function (Request $request) {
    dd(Request::getClientIp());
});
Route::get('test', function () {
    return SMS::test();
});
Route::get('r', function()
{
    header('Content-Type: application/excel');
    header('Content-Disposition: attachment; filename="routes.csv"');

    $routes = Route::getRoutes();
    $fp = fopen('php://output', 'w');
    fputcsv($fp, ['METHOD', 'URI', 'NAME', 'ACTION']);
    foreach ($routes as $route) {
        fputcsv($fp, [head($route->methods()) , $route->uri(), $route->getName(), $route->getActionName()]);
    }
    fclose($fp);
});
//Auth Routes
// Authentication Routes...
$this->get('/login', 'Auth\AuthController@showLoginForm');
$this->post('login', 'Auth\AuthController@login');
$this->get('logout', 'Auth\AuthController@logout');

// Registration Routes...
$this->get('register', 'Auth\AuthController@showRegistrationForm')->middleware('auth');
$this->post('register', 'Auth\AuthController@register')->middleware('auth');

// Password Reset Routes...
$this->get('password/reset/{token?}', 'Auth\PasswordController@showResetForm')->middleware('auth');
$this->post('password/email', 'Auth\PasswordController@sendResetLinkEmail')->middleware('auth');
$this->post('password/reset', 'Auth\PasswordController@reset')->middleware('auth');

Route::get('/unit/showEntries', 'UnitController@index')->middleware('auth');
Route::get('/unit/showTotalEntries/{match_id?}', ['as'=>"showTotalEntries",'uses'=>'UnitController@showTotalEntries'])->middleware('NRAIUser');
