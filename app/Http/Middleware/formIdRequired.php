<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Session;

class formIdRequired
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->session()->has('form_id'))
            return redirect(route('formRoot'));
        else{
            $form = \App\Form::findOrFail(Session::get('form_id'));
            if($form->match->last_date < \Carbon\Carbon::now())
            {
                return redirect(route('formRoot'));
            }
        }
        return $next($request);
    }
}
