<?php

namespace App\Http\Middleware;

use Closure;

class NRAIUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //Check if the user is logged in .. hence check if user exixts
        if ($request->user()) {
            //check if the user is super admin
            if ($request->user()->id == 1) {
                return $next($request);
            }
        }
        return response('UnAuthorized', 403);
    }
}
