<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use SebastianBergmann\Comparator\ArrayComparatorTest;

class Event extends Model
{
    //
    protected $fillable = [
        'match_id',
        'name',
        'class',
        'type',
        'gender',
        'nat_civil',
        'category',
        'issf_qualification_score',
        'nr_qualification_score',
        'max_score',
        'consider_for_qualification',
        'fees'
    ];
    protected $casts = [
        'consider_for_qualification' => 'boolean'
    ];
    protected static $decodeArray = Array(
        ('classes') => Array(
            '10M Rifle',
            '10M Pistol',
            '50M Rifle 3 Position',
            '50M Rifle Prone',
            '25M Center Fire Pistol',
            '25M Rapid Fire Pistol',
            '25M Pistol',
            '25M Sports Pistol',
            '25M Standard Pistol',
            '50M Pistol',
            'Clay Pigeon Trap',
            'Clay Pigeon Double Trap',
            'Clay Pigeon Skeet',
            '300M Rifle Prone',
            '300M Rifle 3 Position',
            '300M Standard Rifle 3 Position'
        ),
        ('categories') => Array(
            'Senior',
            'Junior',
            'Youth',
            'Handicapped',
            'Veterans',
            'Services',
            'Junior Services',
            'MQS'
        ),
        ('nat_civil') => Array(
            'National',
            'Civilian',
            '-'
        ),
        ('types') => Array(
            'ISSF',
            'NR',
            'FOREIGN NATIONAL'
        ),
        ('genders') => Array(
            'Men',
            'Women',
            'Common'
        )

    );

    public function match()
    {
        return $this->belongsTo('App\Match');
    }

    public function scores()
    {
        return $this->hasMany('App\Score');
    }


    public function scopeForMatch($query,$match_id){
        return $query->where('match_id',$match_id);
    }
    public function scopeForClasses($query,$playableClasses){
        return $query->where('classes' , 'in',$playableClasses);
    }
    public function setNameAttribute($name)
    {
        $this->attributes['name'] = strtoupper($name);
    }

    public function setClassAttribute($class)
    {
        if (in_array($class, array_get(\App\Event::decodeArray(), 'classes'))) {
            $this->attributes['class'] = $class;
        } else return "error";
    }

    public function setTypeAttribute($type)
    {
        if (in_array($type, array_get(\App\Event::decodeArray(), 'types'))) {
            $this->attributes['type'] = $type;
        } else return "error";
    }

    public function setGenderAttribute($gender)
    {
        if (in_array($gender, array_get(\App\Event::decodeArray(), 'genders'))) {
            $this->attributes['gender'] = $gender;
        } else return "error";
    }

    public function setNatCivilAttribute($nat_civil)
    {
        if (in_array($nat_civil, array_get(\App\Event::decodeArray(), 'nat_civil'))) {
            $this->attributes['nat_civil'] = $nat_civil;
        } else return "error";
    }

    public function setCategoryAttribute($category)
    {
        if (in_array($category, array_get(\App\Event::decodeArray(), 'categories'))) {
            $this->attributes['category'] = $category;
        } else return "error";
    }

    public function setIssfQualificationScoreAttribute($value)
    {
        //When Its not set by user
        if($value == 0 or $value == 0.00 or $value == "" or $value == null) {
            $issfQualScore = \App\QualScore::getQualScore(
                $this->attributes['class'],
                $this->attributes['type'],
                $this->attributes['gender'],
                $this->attributes['category']
            );
            $this->attributes['issf_qualification_score'] = $issfQualScore;
        }
    }
    public function setNrQualificationScoreAttribute($value)
    {
        //When Its not set by user
        if($value == 0 or $value == 0.00 or $value == "" or $value == null) {
            $issfQualScore = \App\QualScore::getQualScore(
                $this->attributes['class'],
                'ISSF',
                $this->attributes['gender'],
                $this->attributes['category']
            );
            $this->attributes['nr_qualification_score'] = $issfQualScore;
        }
    }
    public function setMaxScoreAttribute($value)
    {
        //When Its not set by user
        if($value == 0 or $value == 0.00 or $value == "" or $value == null) {
            $max_score = \App\QualScore::MaxScore(
                $this->attributes['class'],
                'NR',
                $this->attributes['gender'],
                $this->attributes['category']
            );
            $this->attributes['max_score'] = $max_score;
        }
    }

    /**
     * @return array Entire Information
     */
    public static function decodeArray()
    {
        return self::$decodeArray;
    }

    /**
     * @param $match_id ID of match you want information of
     * @param string $requirement What You want eg "type" , "gender" ,"category" etc
     * @return array Information for mentioned Match ID
     */
    public static function decodeArrayForMatch_id($match_id,$requirement = ""){
        $decodeArray = \App\Event::decodeArray();
        switch ($requirement){
            case "classes":
                $classes = \App\Event::where('match_id',$match_id)->distinct()->lists('class')->toArray();
                foreach($decodeArray['classes'] as $classKey => $classValue){
                    if(in_array($classValue,$classes))  continue;
                    else unset($decodeArray['classes'][$classKey]);
                }
                return $decodeArray['classes'];
            break;
            case "genders":
                $genders = \App\Event::where('match_id',$match_id)->distinct()->lists('gender')->toArray();
                foreach($decodeArray['genders'] as $genderKey=>$genderValue){
                    if(in_array($genderValue,$genders))  continue;
                    else unset($decodeArray['genders'][$genderKey]);
                }
            return $decodeArray['genders'];
            break;
            case "categories":
                $categories = \App\Event::where('match_id',$match_id)->distinct()->lists('category')->toArray();
                foreach($decodeArray['categories'] as $categoryKey => $categoryValue){
                    if(in_array($categoryValue,$categories))  continue;
                    else unset($decodeArray['categories'][$categoryKey]);
                }
            return $decodeArray['categories'];
            break;
            case "types" :
                $types = \App\Event::where('match_id',$match_id)->distinct()->lists('type')->toArray();
                foreach($decodeArray['types'] as $typeKey => $typeValue){
                    if(in_array($typeValue,$types))  continue;
                    else unset($decodeArray['types'][$typeKey]);
                }
            return $decodeArray['types'];
            break;
            case "nat_civil":
                $nat_civil = \App\Event::where('match_id',$match_id)->distinct()->lists('nat_civil')->toArray();
                foreach($decodeArray['nat_civil'] as $nat_civilKey => $nat_civilValue){
                    if(in_array($nat_civilValue,$nat_civil))  continue;
                    else unset($decodeArray['nat_civil'][$nat_civilKey]);
                }
                return $decodeArray['nat_civil'];
                break;
            default :
                $classes = \App\Event::where('match_id',$match_id)->distinct()->lists('class')->toArray();
                foreach($decodeArray['classes'] as $classKey => $classValue){
                    if(in_array($classValue,$classes))  continue;
                    else unset($decodeArray['classes'][$classKey]);
                }
                $genders = \App\Event::where('match_id',$match_id)->distinct()->lists('gender')->toArray();
                foreach($decodeArray['genders'] as $genderKey=>$genderValue){
                    if(in_array($genderValue,$genders))  continue;
                    else unset($decodeArray['genders'][$genderKey]);
                }
                $categories = \App\Event::where('match_id',$match_id)->distinct()->lists('category')->toArray();
                foreach($decodeArray['categories'] as $categoryKey => $categoryValue){
                    if(in_array($categoryValue,$categories))  continue;
                    else unset($decodeArray['categories'][$categoryKey]);
                }
                $types = \App\Event::where('match_id',$match_id)->distinct()->lists('type')->toArray();
                foreach($decodeArray['types'] as $typeKey => $typeValue){
                    if(in_array($typeValue,$types))  continue;
                    else unset($decodeArray['types'][$typeKey]);
                }
                $nat_civil = \App\Event::where('match_id',$match_id)->distinct()->lists('nat_civil')->toArray();
                foreach($decodeArray['nat_civil'] as $nat_civilKey => $nat_civilValue){
                    if(in_array($nat_civilValue,$nat_civil))  continue;
                    else unset($decodeArray['nat_civil'][$nat_civilKey]);
                }
                return $decodeArray;
            break;

        }
    }

    public static function decodeEvent($matchName, $requirement)
    {
        print_r($matchName . "\n" . $requirement);
        $decodeArray = \App\Event::decodeArray();
        $requiredArray = array_get($decodeArray, $requirement);
        foreach ($requiredArray as $key => $individualMatchName) {
            if (stripos($matchName, $individualMatchName) !== FALSE) {
                // found
                return $requiredArray[$key];
            }
        }
        if ($requirement == 'nat_civil') return '-';
        if ($requirement == 'genders') return 'Common';
        if ($requirement == 'categories') return 'Senior';
        if ($requirement == 'classes' && strpos($matchName, 'DOUBLE TRAP') !== FALSE)
            return 'Clay Pigeon Double Trap';
        if ($requirement == 'classes' && strpos($matchName, 'SKEET') !== FALSE)
            return 'Clay Pigeon Skeet';
        echo "Error in decoding {$matchName} of {$requirement}";
    }
}
