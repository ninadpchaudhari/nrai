<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    //Dates
    protected $dates = ['start_date','end_date','last_date'];
    protected $fillable = ['name','place','start_date','end_date','short_name','year','last_date','stateVerificationRequired'];

    public function setStartDateAttribute($date){
        $this->attributes['start_date'] = Carbon::parse( $date);
        //$this->attributes['start_date'] = Carbon::createFromFormat('d-m-Y', $date);
    }
    public function setEndDateAttribute($date){
        $this->attributes['end_date'] = Carbon::parse($date);
        //$this->attributes['end_date'] = Carbon::createFromFormat('d-m-Y', $date);
    }
    public function setLastDateAttribute($date){
        $proper_date = Carbon::parse($date);
        $proper_date->addHours(23);
        $proper_date->addMinutes(59);
        $this->attributes['last_date'] = $proper_date;
    }

    public function setStateVerificationRequiredAttribute($value)
    {
        if($value != true || $value != false)
        {
            if($value == 'on')
                $this->attributes['stateVerificationRequired'] = true;
            else
                $this->attributes['stateVerificationRequired'] = false;
        }

    }
    public function getLastDateAttribute($date){
        return Carbon::parse($date);
    }

    //Match Has many Events
    public function events(){
        return $this->hasMany('App\Event');
    }
    public function forms(){
        return $this->hasMany('App\Form','match_id','id');
    }

}
