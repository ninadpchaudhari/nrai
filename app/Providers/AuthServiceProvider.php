<?php
namespace App\Providers;

use Illuminate\Support\Facades\Log;
use DebugBar\DebugBar;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {


        $gate->before(function($user){
            //dd("hi");
            if($user->isSuperAdmin()){
                Log::info("SuperAdmin Authorized for something");
                return true;
            }
            return false;
        });
        $gate->define('register-users', function ($user, \App\User $newUser) {
            
            return $user->hasRightFor('register-users');
        });
        $this->registerPolicies($gate);

    }
}