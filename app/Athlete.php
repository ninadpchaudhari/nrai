<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Athlete extends Model
{
    //
    protected $dates = ['DOB'];



    public function scopeSelectPublicInfo($query){
        return $query->select([
            'shooterID',
            'shooterName',
            'motherName',
            'fatherName',
            'state',
            'stateOfRep',
            'eventRifle',
            'eventPistol',
            'eventShotgun',
            'sex',
            'DOB' // Removed later -- Required for YOB
        ]);
    }

    //Athlete has many scores
    public function scores(){
        return $this->hasMany('App\Score','shooterID','shooterID');
    }

    public function events($match_id,$shooterID){
        return DB::table('events')
            ->join('scores','scores.event_id','=','events.id')
            ->select('events.*','scores.shooterID')
            ->where(['scores.shooterID'=> $shooterID,'events.match_id'=>$match_id])
            ->get();
    }


    //mutators
    public function getShooterIDAttribute($shooterID){
        return strtoupper($shooterID);
    }


}
