<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProvisionalScore extends Model
{
    protected $casts = [
        'shots' =>'array',
        'series'=>'array'
    ];
    //
    public function match(){
        return $this->belongsTo('App\Match');
    }

    public function setShotsAttribute($value){
        $tempSeries = [];
        foreach($value as $i=>$tenShots){
            if (is_numeric($tenShots)) {
                //Series isnt a array , but only 1 number .
                //Assign the series values and then continue with the loop
                $tempSeries[$i] = $tenShots;
            } else {
                //Do total and then save to Series array
                $tempSeries[$i] = array_sum($tenShots);
            }
        }
        $this->series = $tempSeries;
    }
    
}
