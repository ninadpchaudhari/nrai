<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FinalScore extends Model
{
    protected $casts = [
        'shots' => 'array',
    ];

}
